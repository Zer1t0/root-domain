.PHONY: install, install-requirements

install-requirements:
	/usr/bin/env pip3 install -r requirements.txt

install:
	/usr/bin/env python3 setup.py install

